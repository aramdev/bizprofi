import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import selectedItems from './modules/selected.items';
import selectItems from './modules/select.items';


export default new Vuex.Store({
  modules: {
    selectedItems,
    selectItems
  },
  strict: process.env.NODE_ENV !== 'production'
})
