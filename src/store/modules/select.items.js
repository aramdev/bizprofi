export default {
    namespaced: true,
    state: {
        items: [],
    },
    getters: {
        list(state, getters){
            return state.items
        },
    },
    mutations: {
        set(state, data){
            state.items = data
        },
    },
    actions: {
        load(store){
            let data = [];
            for(let i = 1; i <= 300; i++){
                data.push({
                    id: i,
                    title: `Элемент ${i}`
                })
            }

            store.commit('set', data);
        }
    }
}


