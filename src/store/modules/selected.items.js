export default {
    namespaced: true,
    state: {
        items: [],
        
    },
    getters: {
        count(state, getters){
            return state.items.length
        },
        list(state, getters){
            return state.items
        },
    },
    mutations: {
        remove(state, data){
            let ind = data.num
            state.items.splice(ind, 1);
        },
        set(state, data){
            state.items = data
        },

    },
    actions: {
        load(store){
            let temp = [];
            while(temp.length < 3){
                let random = Math.floor(Math.random() * 300) + 1
                let ind = temp.indexOf(random)
                if(ind < 0){
                    temp.push(random) 
                }
            }

            let data = temp.map((el) => {
                return {
                    id: el,
                    title: `Элемент ${el}`
                }
            })
            
            store.commit('set', data);
        },
        
        remove(store, data){
            store.commit('remove', data);
        },

        saveSelected(store, data){
            store.commit('set', data);
        }
    }
}