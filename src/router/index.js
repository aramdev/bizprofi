import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '~v/Home.vue'
import SelectedItems from '~v/SelectedItems.vue'
import SelectItems from '~v/SelectItems.vue'
import SelectedSelectItems from '~v/SelectedSelectItems.vue'
import CreateStore from '~v/CreateStore.vue'
import ItemReport from '~v/ItemReport.vue'
import E404 from '~v/E404.vue'

Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Главная'
    }
  },
  {
    path: '/selected-items',
    name: 'selectedItems',
    component: SelectedItems,
    meta: {
      title: 'Компонент отображения выбранных элементов'
    }
  },
  {
    path: '/select-items',
    name: 'selectItems',
    component: SelectItems,
    meta: {
      title: 'Компонент выбора элементов'
    }
  },
  {
    path: '/selected-select-items',
    name: 'selectedSelectItems',
    component: SelectedSelectItems,
    meta: {
      title: 'Обеденение компонентов SelectedItems SelectItems'
    }
  },
  {
    path: '/create-store',
    name: 'createStore',
    component: CreateStore,
    meta: {
      title: 'Создать хранилище данных'
    }
  },
  {
    path: '/item-report',
    name: 'itemReport',
    component: ItemReport,
    meta: {
      title: 'Отчет по элементам'
    }
  },

  //
  {
    path: '*',
    name: 'E404',
    component: E404,
    meta: {
      title: 'E404'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
